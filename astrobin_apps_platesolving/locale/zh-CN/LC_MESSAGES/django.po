# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-01-11 03:17+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: templates/astrobin_apps_platesolving/inclusion_tags/platesolving_machinery.html:10
msgid "Getting plate-solving status, please wait..."
msgstr ""

#: templates/astrobin_apps_platesolving/inclusion_tags/platesolving_machinery.html:28
msgid "Plate-solving process starting, please do not leave this page..."
msgstr ""

#: templates/astrobin_apps_platesolving/inclusion_tags/platesolving_machinery.html:29
msgid "Plate-solving started in the background. You may leave this page."
msgstr ""

#: templates/astrobin_apps_platesolving/inclusion_tags/platesolving_machinery.html:30
msgid "Plate-solving almost ready, please do not leave this page..."
msgstr ""

#: templates/astrobin_apps_platesolving/inclusion_tags/platesolving_machinery.html:31
msgid "This image could not be plate-solved."
msgstr ""

#: templates/astrobin_apps_platesolving/inclusion_tags/platesolving_machinery.html:32
msgid ""
"This image has been successfully plate-solved. Please refresh the page to "
"see the new data."
msgstr ""
