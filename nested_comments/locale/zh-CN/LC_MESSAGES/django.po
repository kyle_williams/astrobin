# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-01-11 03:15+0000\n"
"PO-Revision-Date: 2014-04-29 13:19+0000\n"
"Last-Translator: ecloud <yecloud@me.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh-CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Pootle 2.5.0\n"
"X-POOTLE-MTIME: 1398777561.828400\n"

#: templates/nested_comments/nestedcomment_app.html:23
#: templates/nested_comments/nestedcomment_app.html:138
#: templates/nested_comments/nestedcomment_app.html:161
#: templates/nested_comments/nestedcomment_list.html:58
msgid "Cancel"
msgstr "取消"

#: templates/nested_comments/nestedcomment_app.html:38
msgid "Add a comment"
msgstr "添加评论"

#: templates/nested_comments/nestedcomment_app.html:43
#: templates/nested_comments/nestedcomment_app.html:177
msgid ""
"Please <a href=\"/accounts/login/\">login</a> or <a href=\"/accounts/"
"register/\">register</a> to leave a comment.</p>"
msgstr ""
"请<a href=\"/accounts/login/\">登录</a> 或 <a href=\"/accounts/register/\">注"
"册</a> 来进行评论</p>"

#: templates/nested_comments/nestedcomment_app.html:52
msgid "There are no comments yet."
msgstr "现在还没有评论"

#: templates/nested_comments/nestedcomment_app.html:68
#: templates/nested_comments/nestedcomment_app.html:86
msgid "(deleted)"
msgstr "（已删除）"

#: templates/nested_comments/nestedcomment_app.html:97
#: templates/nested_comments/nestedcomment_list.html:33
msgid "Link"
msgstr "链接"

#: templates/nested_comments/nestedcomment_app.html:101
#: templates/nested_comments/nestedcomment_list.html:39
msgid "Undelete"
msgstr "恢复删除"

#: templates/nested_comments/nestedcomment_app.html:103
#: templates/nested_comments/nestedcomment_list.html:41
msgid "Delete"
msgstr "删除"

#: templates/nested_comments/nestedcomment_app.html:108
#: templates/nested_comments/nestedcomment_list.html:44
msgid "Edit"
msgstr "编辑"

#: templates/nested_comments/nestedcomment_app.html:113
#: templates/nested_comments/nestedcomment_list.html:46
msgid "Reply"
msgstr "回复"

#: templates/nested_comments/nestedcomment_app.html:141
#: templates/nested_comments/nestedcomment_app.html:164
msgid "The comment cannot be empty."
msgstr "评论不能是空的"

#: templates/nested_comments/nestedcomment_app.html:188
#: templates/nested_comments/nestedcomment_list.html:57
msgid "Save"
msgstr "保存"

#: templates/nested_comments/nestedcomment_app.html:198
msgid "Preview"
msgstr "预览"

#: templates/nested_comments/nestedcomment_app.html:204
#: templates/nested_comments/nestedcomment_list.html:60
msgid "Formatting help"
msgstr "格式使用帮助"

#: templates/nested_comments/nestedcomment_app.html:208
msgid "You type:"
msgstr "您键入："

#: templates/nested_comments/nestedcomment_app.html:209
msgid "You see:"
msgstr "您会看到："

#: templates/nested_comments/nestedcomment_app.html:214
#: templates/nested_comments/nestedcomment_app.html:215
msgid "italics"
msgstr "斜体"

#: templates/nested_comments/nestedcomment_app.html:219
#: templates/nested_comments/nestedcomment_app.html:220
msgid "bold"
msgstr "粗体"

#: templates/nested_comments/nestedcomment_app.html:229
msgid "(TAB or 4 spaces)"
msgstr "（TAB按钮或者4个空格）"

#: templates/nested_comments/nestedcomment_app.html:229
#: templates/nested_comments/nestedcomment_app.html:230
msgid "preformatted text"
msgstr "预格式文本"

#: templates/nested_comments/nestedcomment_app.html:234
#: templates/nested_comments/nestedcomment_app.html:235
msgid "quoted text"
msgstr "引用的文字"

#: templates/nested_comments/nestedcomment_app.html:239
#: templates/nested_comments/nestedcomment_app.html:240
msgid "escape the formatting syntax"
msgstr "转义格式化的变量"

#: templates/nested_comments/nestedcomment_list.html:26
msgid "(comment deleted)"
msgstr "（评论已删除）"

#: templates/nested_comments/nestedcomment_list.html:75
msgid "There doesn't seem to be anything here."
msgstr "似乎没有任何东西在这里。"
